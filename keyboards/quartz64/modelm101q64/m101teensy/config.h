/*
Copyright 2019 DmNosachev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "config_common.h"

#define DEVICE_VER 0x0001

#define MATRIX_ROW_PINS { D0, D1, D2, D3, D4, D5, D6, D7 }
#define MATRIX_COL_PINS { B6, B5, B4, B3, B2, B1, B0, E7, E6, F0, F1, F2, F3, F4, F5, F6 }

#define SCROLL_LOCK_LED_PIN C1
#define NUM_LOCK_LED_PIN C2
#define CAPS_LOCK_LED_PIN C0
